; $Id$

core = 6.x

; Patched
;projects[ctools][subdir] = "contrib"
;projects[ctools][version] = 1.3
;projects[ctools][patch][] = "http://drupal.org/files/issues/716288-1_clear_caches.patch"

; Contrib projects
projects[storm][subdir] = "contrib"
projects[storm][version] = 1.36
projects[storm_dashboard][subdir] = "contrib"
projects[storm_dashboard][version] = 1.0
projects[storm_quicktt][subdir] = "contrib"
projects[storm_quicktt][subdir] = 1.0
